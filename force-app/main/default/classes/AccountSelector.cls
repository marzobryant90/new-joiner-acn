public with sharing class AccountSelector
{
    public AccountSelector() {}

    public static Object getData(Boolean isBatchContext, String fieldSetName, String objectName)
    {
        if(isBatchContext)
        {
            return Database.getQueryLocator(getQuery(fieldSetName, objectName));
        }
        else
        {
            return Database.query(getQuery(fieldSetName, objectName));            
        }
    }

    /**
    *    @Description: This method returns the query string
    *    @Arguments: FieldSet Name, Object Name
    *    @Return: String
    **/
    public static String getQuery(String fieldSetName, String objectName)
    {
        String query = 'SELECT ';
        
        for(Schema.FieldSetMember f : readFieldSet(fieldSetName, objectName))
        {
            query += f.getFieldPath() + ', ';
        }

        query += 'Id FROM ' + objectName;

        return query;
    }

    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String objectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        return fieldSetObj.getFields(); 
    }  
}
