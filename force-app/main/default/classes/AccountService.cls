public with sharing class AccountService {
    
    public static void sendAccountData()
    {
        List<Account> accounts = (List<Account>) AccountSelector.getData(false, 'batchFieldset', 'Account');

        // Example
        
        /*
        {
            "Name" : "Pippo",
            "Mail" : "xxx@x.com"
        }
        */

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStartArray();

        for(Account a : accounts)
        {
            gen.writeStartObject();
            gen.writeStringField('Name', a.Name);
            gen.writeStringField('Mail', a.Email__c);
            gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
        String jsonS = gen.getAsString();

        System.debug('jsonMaterials'+jsonS);
    }
}
