global with sharing class BatchCleaningAccount implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return (Database.QueryLocator) AccountSelector.getData(true, 'batchFieldset', 'Account');
    }

    global void execute(Database.BatchableContext BC, List<Account> accounts)
    {
        for(Account a : accounts)
        {
            a.Email__c = 'xxxx@xxxxx.x';
        }

        update accounts;
    }

    global void finish(Database.BatchableContext BC){}
}