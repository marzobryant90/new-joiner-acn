public with sharing class AccountTriggerHandler
{
    public static void prettifyData(List<Account> accounts)
    {
        for(Account a : accounts)
        {
            if(!String.isBlank(a.Phone))
            {
                if(!a.Phone.startsWith('+39'))
                {
                    a.Phone = '+39' + a.Phone;
                }
                a.Phone = a.Phone.trim().replace(' ', '');
            }
        }
    }
}
