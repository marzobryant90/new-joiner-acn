trigger AccountTrigger on Account (before insert, before update)
{
    if(Trigger.isBefore)
    {
        if(Trigger.isInsert)
        {
            AccountTriggerHandler.prettifyData(Trigger.new);
        }
    }

    if(Trigger.isBefore)
    {
        if(Trigger.isUpdate)
        {
            AccountTriggerHandler.prettifyData(Trigger.new);
        }
    }
}